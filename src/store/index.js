import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    results: [],
    currentResult: [
      { name: "性別", answer: "" },
      { name: "項目1", answer: "" },
      { name: "項目2", answer: "" },
      { name: "項目3", answer: "" },
      { name: "項目4", answer: "" },
      { name: "項目5", answer: "" },
      { name: "項目6", answer: "" },
      { name: "項目7", answer: "" },
      { name: "項目8", answer: "" },
      { name: "項目9", answer: "" },
      { name: "項目10", answer: "" },
      { name: "項目11", answer: "" },
      { name: "項目12", answer: "" },
      { name: "項目13", answer: "" },
      { name: "項目14", answer: "" },
      { name: "項目15", answer: "" }
    ],
    currentRowIndex: 0,
    currentPageIndex: 0,
    isOpenDrawer: false
  },
  mutations: {
    addAnswerSet(state, answerSet) {
      state.answerSets = [
        ...state.answerSets,
        answerSet
      ]
    },
    setAnswer(state, payload) {
      state.currentResult[state.currentRowIndex].answer = payload
    },
    addResult(state) {
      state.results = [
        ...state.results,
        state.currentResult
      ]
    },
    resetResult(state) {
      state.currentResult = state.currentResult.map(item => ({ name: item.name, answer: "" }));
    },
    proceedRowIndex(state) {
      state.currentRowIndex = state.currentRowIndex === state.currentResult.length ? state.currentResult.length : state.currentRowIndex + 1;
    },
    proceedPageIndex(state) {
      state.currentPageIndex = state.currentPageIndex + 1;
    },
    returnRowIndex(state) {
      state.currentRowIndex = state.currentRowIndex === 0 ? 0 : state.currentRowIndex - 1;
    },
    resetRowIndex(state) {
      state.currentRowIndex = 0;
    },
    toggleIsOpenDrawer(state) {
      state.isOpenDrawer = !state.isOpenDrawer;
    }
  }
})
